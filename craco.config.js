const path = require('path');

module.exports = {
  plugins: [{ plugin: require('@semantic-ui-react/craco-less') }],
  webpack: {
    // resolve: {
    //   modules: [
    //     path.resolve(__dirname, 'src'),
    //   ],
    // },
    configure: (webpackConfig, { env, paths }) => {
      if (!webpackConfig.resolve) {
        webpackConfig.resolve = {
          modules: [],
        };
      }

      webpackConfig.resolve.modules.push(path.resolve(__dirname, 'src'));

      return webpackConfig;
    },
  },
};
