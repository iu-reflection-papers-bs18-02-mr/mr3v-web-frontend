import React, {useEffect, useMemo, useRef} from 'react';

import * as THREE from 'three';

import { extend } from 'react-three-fiber';
import { useThree, useFrame } from 'react-three-fiber';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { OutlinePass } from 'three/examples/jsm/postprocessing/OutlinePass';
import { ShaderPass } from "three/examples/jsm/postprocessing/ShaderPass";
import { TAARenderPass } from 'three/examples/jsm/postprocessing/TAARenderPass.js';
extend({ EffectComposer, RenderPass, OutlinePass, ShaderPass, TAARenderPass });

function Effects(props) {
  const { gl, scene, camera, size } = useThree();
  const effectComposer = useRef();

  useEffect(() => effectComposer.current.setSize(size.width, size.height), [size]);
  useFrame(() => effectComposer.current.render(), 1);

  return (
    <effectComposer ref={effectComposer} args={[gl]}>
      <renderPass attachArray="passes" args={[scene, camera]} />
      {props.children}
    </effectComposer>
  );
}

function OutlineEffect(props) {
  const { scene, camera, size } = useThree();

  const aspect = useMemo(() => new THREE.Vector2(size.width, size.height), [size]);

  return (
    <outlinePass
      attachArray="passes"
      args={[aspect, scene, camera]}
      selectedObjects={props.model}
      visibleEdgeColor={props.visibleEdgeColor}
      hiddenEdgeColor={props.hiddenEdgeColor}
      edgeStrength={10}
      edgeThickness={1}
    />
  );
}

export default class LoadedModel extends React.Component {
  hoveredVisibleColor = 0xFFFFFF;
  hoveredHiddenColor = 0xFCBF1E;
  selectedVisibleColor = 0x2662F0;
  selectedHiddenColor = 0xFCBF1E;

  render() {
    if (this.props.loadedModel) {
      var axis_exists = false;
      var grid_exists = false;
      this.props.loadedModel.scene.children.forEach(child => {
        if (child instanceof THREE.AxesHelper) {
          axis_exists = true;
        }
        if (child instanceof THREE.GridHelper) {
          grid_exists = true;
        }
      });

      const selectedModels = this.props.selectedModels;
      const hoveredModel =
        (
          this.props.hoveredModel &&
          selectedModels.every(model =>
            model.uuid !== this.props.hoveredModel.uuid
          )
        ) ?
          [this.props.hoveredModel] :
          []
      ;


      var bbox = new THREE.Box3().setFromObject( this.props.loadedModel.scene );

      const displayEffects =
        ((selectedModels.length > 0) || (hoveredModel.length > 0));

      const modelToDisplay = this.props.loadedModel.scene;
      return (
        <>
          <primitive object={ modelToDisplay }/>
          {
            !axis_exists &&
            <primitive object={ new THREE.AxesHelper() } />
          }
          {
            !grid_exists &&
            <primitive object={ new THREE.GridHelper(bbox.getSize().x * 10, 10) } />
          }
          {
            (displayEffects) &&
            <Effects>
              {
                (selectedModels.length > 0) &&
                <OutlineEffect
                  model={selectedModels}
                  visibleEdgeColor={this.selectedVisibleColor}
                  hiddenEdgeColor={this.selectedHiddenColor}
                />
              }
              {
                (hoveredModel.length > 0) &&
                <OutlineEffect
                  model={hoveredModel}
                  visibleEdgeColor={this.hoveredVisibleColor}
                  hiddenEdgeColor={this.hoveredHiddenColor}
                />
              }
            </Effects>
          }
        </>
      );
    }
    else {
      return <></>;
    }
  }
}
