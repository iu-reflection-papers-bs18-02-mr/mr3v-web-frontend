import React from 'react';

import TreeNode from './TreeNode';

import AddIcon from './add.png';

import './style.css';

export default class ModelTree extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedElementId: null,
      deletedElementId: null,
    };
  }

  handleElementSelect = (model) => {
    this.setState({ selectedElementId: model.uuid });
    this.props.onElementSelect(model);
  }

  handleElementUnselect = (model) => {
    this.setState({ selectedElementId: null });
    this.props.onElementUnselect(model);
  }

  handleElementDelete = (model) => {
    this.setState({ deletedElementId: model.uuid});
    console.log(model.uuid);
    this.props.onElementDelete(model);
  }

  render() {
    return (
      <div className="model-tree">
        <div className="model-tree-title">
          <h3>Model tree</h3>
        </div>

        <div className="model-tree-nodes">
          {this.props.model &&
          this.props.model.scene.children.map(child => {
            return (
              <TreeNode
                key={child.uuid}
                selectedElementId={this.state.selectedElementId}
                depth={0}
                model={child}
                onElementMouseOver={this.props.onElementMouseOver}
                onElementMouseOut={this.props.onElementMouseOut}
                onElementSelect={this.handleElementSelect}
                onElementUnselect={this.handleElementUnselect}
                onElementDelete={this.handleElementDelete}
              />
            );
          })}
        </div>
        <img src={AddIcon} alt="add icon" width="25px" height="25px" float="left"></img>
      </div>
    );
  }
}
